mod file_load;
mod conversion;
mod printer;

use std::env;

fn process_image(path: String) {
    let image = file_load::load_image(&path);
    let output = conversion::process_image(&image);
    printer::print_image(&output);
}

fn main() {
    let first_arg = env::args().nth(1);
    match first_arg {
        Some(path) => process_image(path),
        None    => println!("Please provide a filename."),
    }
}
