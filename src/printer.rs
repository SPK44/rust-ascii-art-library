pub fn print_image(output_buffer: &Vec<String>) {
    for line in output_buffer.iter() {
        println!("{:?}", line);
    }
}
