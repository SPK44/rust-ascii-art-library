extern crate image;

//mod output_buffer;

use std::convert::TryFrom;

const PALLETE: [char; 15] = [' ', '`', '.', '+', '-', '*', '#', 'X', '@', '%', '&', '$', 'M', 'W', '8'];

fn create_thresholds(length: usize) -> Vec<u8> {
    let length_u8 = u8::try_from(length).unwrap();
    assert_eq!(u8::MAX.checked_rem(length_u8), Some(0));
    let interval = u8::MAX.div_euclid(length_u8);
    let mut thresholds = Vec::with_capacity(length);

    for i in 1..=length_u8 {
        thresholds.push(i*interval);
    }

    return thresholds;
}

pub fn process_image(image: &image::GrayImage) -> Vec<String> {
    let thresholds = create_thresholds(PALLETE.len());

    let (u32width, u32height) = image.dimensions();
    let width = usize::try_from(u32width).unwrap();
    let height = usize::try_from(u32height).unwrap();
    let mut output = vec![String::with_capacity(width); height + 1];

    for (u32y, row) in image.enumerate_rows() {
        let y = usize::try_from(u32y).unwrap();

        for (_x, _y, pixel) in row {
            for index in 0..thresholds.len() {
                if pixel[0] <= thresholds[index] {
                    output[y].push(PALLETE[index]);
                    break;
                }
            }
        }
    }
    output
}
