extern crate image;

pub fn load_image(image_path: &String) -> image::GrayImage {
    let img = image::open(image_path).unwrap();
    img.into_luma8()
}
