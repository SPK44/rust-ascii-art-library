extern crate term;

use term::color;

#[derive(Copy, Clone)]
struct Pixel {
    glyph: char,
    fg_color: color::Color,
    bg_color: color::Color,
}

impl Pixel {
    fn new_empty() -> Pixel {
        Pixel {
            glyph: '\0',
            fg_color: color::BLACK,
            bg_color: color::BLACK,
        }
    }
}

struct OutputBuffer {
    pixels: Vec<Vec<Pixel>>,
    is_greyscale: bool,
}

impl OutputBuffer {
    fn new(width: usize, height: usize, is_greyscale: bool) -> OutputBuffer {
        let pixels: Vec<Vec<Pixel>> = vec![vec![Pixel::new_empty(); width]; height];
        OutputBuffer {
            pixels,
            is_greyscale,
        }
    }
}
